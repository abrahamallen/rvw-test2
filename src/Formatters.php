<?php
/** @noinspection ALL */


class Formatters
{
    /**
     * Provides consistent formatting for phone numbers.
     *
     * @param string $phone
     * @return string
     */
    public function FormatPhone($phone)
    {
        return '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6);
    }

    /**
     * Returns a singular or plural string, based on the quantity.
     *
     * @param string $singular_string
     * @param int|float $quantity
     * @return string
     */
    public function Pluralize($singular_string, $quantity)
    {
        // TODO: implement this method.
    }
}
